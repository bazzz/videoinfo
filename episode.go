package videoinfo

import (
	"encoding/xml"
	"strconv"

	"gitlab.com/bazzz/dates"
)

// Episode represents a TV show episode.
type Episode struct {
	XMLName   xml.Name   `xml:"episodedetails"`
	Title     string     `xml:"title"`
	Season    int        `xml:"season"`
	Number    int        `xml:"episode"`
	Premiered dates.Date `xml:"aired"`
	Overview  string     `xml:"plot"`
	Image     string     `xml:"-"`
	Show      *Show      `xml:"-"`
}

// NFO returns this episode's nfo file xml represention.
func (e Episode) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(e, "", "\t")
	result := []byte(xml.Header)
	result = append(result, data...)
	return result, err
}

// Filename returns a string suitable to be used as a filename for this episode '<showTitle> S00E00 <title>'.
func (e Episode) Filename() string {
	return stringToFilename(e.Show.Title + " " + e.SeasonAndNumber() + " " + e.Title)
}

func (e Episode) SeasonAndNumber() string {
	season := "S"
	if e.Season <= 9 {
		season += "0"
	}
	season += strconv.Itoa(e.Season)

	number := "E"
	if e.Number <= 9 {
		number += "0"
	}
	number += strconv.Itoa(e.Number)
	return season + number
}
