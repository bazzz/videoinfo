package videoinfo

// Video represents a collection of optional information parsed from a (file)name, plus either a Film or an Episode object (never both).
type Video struct {
	title      string
	year       int
	seasonNr   int
	episodeNr  int
	Film       *Film
	Episode    *Episode
	Proper     bool
	Resolution string
	VideoCodec string
	AudioCodec string
	Source     string
	Language   string
	Releaser   string
	MaybeName  string
}
