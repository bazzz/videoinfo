package videoinfo

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
)

type videoType int

const (
	typeUnknown videoType = iota
	typeEpisode
	typeFilm
)

var (
	yearRegex     *regexp.Regexp
	fulldateRegex *regexp.Regexp
	episodeRegex  *regexp.Regexp
	seasonRegex   *regexp.Regexp
)

func init() {
	r, err := regexp.Compile(`(19|20)\d{2}`)
	if err != nil {
		panic(err)
	}
	yearRegex = r

	r, err = regexp.Compile(`(20|19)\d{2}.\d{2}.\d{2}`)
	if err != nil {
		panic(err)
	}
	fulldateRegex = r

	r, err = regexp.Compile(`[S|s](\d+)[E|e](\d+)|(\d+)[×|x|X](\d+)`)
	if err != nil {
		panic(err)
	}
	episodeRegex = r

	r, err = regexp.Compile(`S\d{2}`)
	if err != nil {
		panic(err)
	}
	seasonRegex = r
}

func parseName(name string) (Video, error) {
	video := Video{}
	videoType, pos := getType(name)
	if videoType == typeUnknown {
		return Video{}, errors.New("could not detect whether this is a TV Show Episode or a Film: " + name)
	}

	switch videoType {
	case typeEpisode:
		episodeIndication := strings.Trim(name[pos[0]:pos[1]], " ")
		episodeIndication = strings.ToLower(episodeIndication)
		seasonNumber := ""
		episodeNumber := ""
		if episodeIndication[0] == 's' && len(episodeIndication) == 3 {
			seasonNumber = episodeIndication[1:3]
			episodeNumber = "0"
		} else if episodeIndication[0] == 's' {
			seasonNumber = episodeIndication[1:3]
			episodeNumber = episodeIndication[4:]
		} else if strings.Contains(episodeIndication, "x") {
			pos := strings.Index(episodeIndication, "x")
			seasonNumber = episodeIndication[0:pos]
			episodeNumber = episodeIndication[pos+1:]
		}
		seasonNr, err := strconv.Atoi(seasonNumber)
		if err != nil {
			return video, err
		}
		video.seasonNr = seasonNr
		episodeNr, err := strconv.Atoi(episodeNumber)
		if err != nil {
			return video, err
		}
		video.episodeNr = episodeNr
	case typeFilm:
		year, err := strconv.Atoi(name[pos[0]:pos[1]])
		if err != nil {
			return video, err
		}
		video.year = year
	}

	video.title = clean(name[:pos[0]])

	tagsString := clean(name[pos[1]:])
	dashPos := strings.LastIndex(tagsString, "-")
	if dashPos > 0 {
		video.Releaser = tagsString[dashPos+1:]
		tagsString = tagsString[:dashPos]
	}
	tags := strings.Split(tagsString, " ")
	for _, tag := range tags {
		switch strings.ToLower(tag) {
		case "proper":
			video.Proper = true
		case "2160p", "1080p", "720p":
			video.Resolution = tag
		case "x264", "h264":
			video.VideoCodec = "H.264"
		case "x265", "hvec":
			video.VideoCodec = "HVEC"
		case "ddp5", "ddp2":
			video.AudioCodec = "DDP"
		case "aac", "aac2":
			video.AudioCodec = "AAC"
		case "nf":
			video.Source = "Netflix"
		case "amzn":
			video.Source = "Amazon"
		case "hdtv":
			video.Source = "HDTV"
		case "webrip", "web-dl":
			video.Source = "Web"
		case "ned", "dutch":
			video.Language = "nl"
		case "ita", "italian":
			video.Language = "it"
		case "spanish":
			video.Language = "es"
		case "fra", "french":
			video.Language = "fr"
		case "kor", "korean":
			video.Language = "ko"
		case "swe", "swedish":
			video.Language = "sw"
		case "german":
			video.Language = "de"
		default:
			if video.Resolution == "" && video.VideoCodec == "" && video.AudioCodec == "" && video.Source == "" && video.Language == "" && video.Releaser == "" {
				video.MaybeName += " " + tag
			}
		}
	}
	video.MaybeName = strings.Trim(video.MaybeName, " ")
	return video, nil
}

func getType(name string) (videoType, []int) {
	pos := episodeRegex.FindStringIndex(name)
	if pos != nil {
		return typeEpisode, pos
	}
	pos = seasonRegex.FindStringIndex(name)
	if pos != nil {
		return typeEpisode, pos
	}
	pos = yearRegex.FindStringIndex(name)
	if pos != nil {
		// Year found, test if it is not a fulldate as films are never marked with a fulldate to indicate their release year.
		test := fulldateRegex.FindStringIndex(name)
		if test == nil { // TODO Maybe year and fullyear should be in the same place?
			return typeFilm, pos
		}
	}
	return typeUnknown, pos
}

func clean(input string) string {
	output := strings.Replace(input, ".", " ", -1)
	output = strings.Trim(output, " \t\n")
	return output
}
