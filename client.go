package videoinfo

import (
	"strconv"
	"strings"
	"time"

	"github.com/patrickmn/go-cache"
	"gitlab.com/bazzz/tmdb"
)

const cacheDuration = 96 * time.Hour

func New(tmdbAPIKey string) (*Client, error) {
	tmdbClient, err := tmdb.New(tmdbAPIKey)
	if err != nil {
		return nil, err
	}
	client := Client{
		tmdbClient: tmdbClient,
		cache:      cache.New(cacheDuration, 1*time.Hour),
	}
	return &client, nil
}

type Client struct {
	tmdbClient *tmdb.Client
	cache      *cache.Cache
}

// SearchShow returns a Show by provided title, and optional ISO3166-1 alpha2 language code.
func (c *Client) SearchShow(name string, language string) (*Show, error) {
	key := name + language
	if show, ok := c.loadShow(key); ok {
		return show, nil
	}
	result, err := c.tmdbClient.SearchShow(name, language)
	if err != nil {
		return nil, err
	}
	if result == nil {
		return nil, nil
	}
	show := &Show{
		Title:      result.Title,
		Premiered:  result.Premiered,
		Genres:     result.Genres,
		Overview:   result.Overview,
		Rating:     result.Rating,
		Votes:      result.Votes,
		Language:   result.OriginalLanguage,
		Poster:     result.Poster,
		Background: result.Backdrop,
		TMDB:       result.ID,
	}
	c.storeShow(key, show)
	return show, nil
}

// GetShow returns a Show by provided TMDB id, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetShow(id int, language string) (*Show, error) {
	key := strconv.Itoa(id) + language
	if show, ok := c.loadShow(key); ok {
		return show, nil
	}
	result, err := c.tmdbClient.GetShow(id, language)
	if err != nil {
		return nil, err
	}
	show := &Show{
		Title:      result.Title,
		Premiered:  result.Premiered,
		Genres:     result.Genres,
		Overview:   result.Overview,
		Rating:     result.Rating,
		Votes:      result.Votes,
		Language:   result.OriginalLanguage,
		Poster:     result.Poster,
		Background: result.Backdrop,
		TMDB:       result.ID,
	}
	c.storeShow(key, show)
	return show, nil
}

// GetEpisode returns an episode with season and number for the provided Show, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetEpisode(show *Show, seasonNr int, episodeNr int, language string) (*Episode, error) {
	key := strconv.Itoa(show.TMDB) + strconv.Itoa(seasonNr) + strconv.Itoa(episodeNr) + language
	if episode, ok := c.loadEpisode(key); ok {
		return episode, nil
	}
	result, err := c.tmdbClient.GetEpisode(show.TMDB, seasonNr, episodeNr, language)
	if err != nil {
		return nil, err
	}
	episode := &Episode{
		Title:     result.Title,
		Season:    result.Season,
		Number:    result.Number,
		Premiered: result.Premiered,
		Overview:  result.Overview,
		Image:     result.Image,
		Show:      show,
	}
	c.storeEpisode(key, episode)
	return episode, nil
}

// SearchFilm returns a Film by provided title, year, and optional ISO3166-1 alpha2 language code.
func (c *Client) SearchFilm(name string, year int, language string) (*Film, error) {
	key := name + strconv.Itoa(year)
	if film, ok := c.loadFilm(key); ok {
		return film, nil
	}
	result, err := c.tmdbClient.SearchFilm(name, year, language)
	if err != nil {
		return nil, err
	}
	if result == nil {
		return nil, nil
	}
	film := &Film{
		Title:      result.Title,
		Premiered:  result.Premiered,
		Overview:   result.Overview,
		Genres:     result.Genres,
		Rating:     result.Rating,
		Popularity: result.Popularity,
		Votes:      result.Votes,
		Language:   result.OriginalLanguage,
		Poster:     result.Poster,
		Background: result.Backdrop,
		TMDB:       result.ID,
	}
	c.storeFilm(key, film)
	return film, nil
}

// GetFilm returns a Film by provided TMDB id, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetFilm(id int, language string) (*Film, error) {
	key := strconv.Itoa(id) + language
	if film, ok := c.loadFilm(key); ok {
		return film, nil
	}
	result, err := c.tmdbClient.GetFilm(id, language)
	if err != nil {
		return nil, err
	}
	film := &Film{
		Title:      result.Title,
		Premiered:  result.Premiered,
		Overview:   result.Overview,
		Genres:     result.Genres,
		Rating:     result.Rating,
		Popularity: result.Popularity,
		Votes:      result.Votes,
		Language:   result.OriginalLanguage,
		Poster:     result.Poster,
		Background: result.Backdrop,
		TMDB:       result.ID,
	}
	c.storeFilm(key, film)
	return film, nil
}

// Resolve parses the provided video (file)name and returns a video object with the results. The video object contains a Film or Episode object with the videoinfo.
func (c *Client) Resolve(name string) (Video, error) {
	video, err := parseName(name)
	if err != nil {
		return video, err
	}
	if video.Film != nil {
		film, err := c.SearchFilm(video.title, video.year, video.Language)
		if err != nil {
			return video, err
		}
		video.Film = film
	} else if video.Episode != nil {
		show, err := c.SearchShow(video.title, video.Language)
		if err != nil {
			return video, err
		}
		episode, err := c.GetEpisode(show, video.seasonNr, video.episodeNr, video.Language)
		if err != nil {
			return video, err
		}
		video.Episode = episode
	}
	return video, nil
}

func stringToFilename(input string) string {
	input = strings.ReplaceAll(input, "\\", "_")
	input = strings.ReplaceAll(input, "/", "_")
	input = strings.ReplaceAll(input, ":", "_")
	input = strings.ReplaceAll(input, "*", "_")
	input = strings.ReplaceAll(input, "?", "_")
	input = strings.ReplaceAll(input, "\"", "_")
	input = strings.ReplaceAll(input, "<", "_")
	input = strings.ReplaceAll(input, ">", "_")
	input = strings.ReplaceAll(input, "|", "_")
	return strings.Trim(input, " \t\n")
}

func (c *Client) loadShow(key string) (*Show, bool) {
	cachekey := "show" + key
	if item, ok := c.cache.Get(cachekey); ok {
		show := item.(*Show)
		return show, true
	}
	return nil, false
}

func (c *Client) storeShow(key string, show *Show) {
	c.cache.SetDefault("show"+key, show)
}

func (c *Client) loadEpisode(key string) (*Episode, bool) {
	cachekey := "episode" + key
	if item, ok := c.cache.Get(cachekey); ok {
		episode := item.(*Episode)
		return episode, true
	}
	return nil, false
}

func (c *Client) storeEpisode(key string, episode *Episode) {
	c.cache.SetDefault("episode"+key, episode)
}

func (c *Client) loadFilm(key string) (*Film, bool) {
	cachekey := "film" + key
	if item, ok := c.cache.Get(cachekey); ok {
		film := item.(*Film)
		return film, true
	}
	return nil, false
}

func (c *Client) storeFilm(key string, film *Film) {
	c.cache.SetDefault("film"+key, film)
}
