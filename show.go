package videoinfo

import (
	"encoding/xml"

	"gitlab.com/bazzz/dates"
)

// Show represents a TV show.
type Show struct {
	XMLName    xml.Name   `xml:"tvshow"`
	Title      string     `xml:"title"`
	Premiered  dates.Date `xml:"premiered"`
	Genres     []string   `xml:"genre"`
	Overview   string     `xml:"plot"`
	Language   string     `xml:"language"`
	Rating     float64    `xml:"-"`
	Votes      int        `xml:"-"`
	Poster     string     `xml:"-"`
	Background string     `xml:"-"`
	TMDB       int        `xml:"-"`
}

// NFO returns this show's nfo file xml represention.
func (s Show) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(s, "", "\t")
	result := []byte(xml.Header)
	result = append(result, data...)
	return result, err
}
