module gitlab.com/bazzz/videoinfo

go 1.17

require (
	github.com/patrickmn/go-cache v2.1.0+incompatible
	gitlab.com/bazzz/dates v0.0.0-20211008090510-94618edb9094
	gitlab.com/bazzz/tmdb v0.0.0-20211009072331-08f358d905c3
)

require github.com/ferhatelmas/levenshtein v0.0.0-20160518143259-a12aecc52d76 // indirect
