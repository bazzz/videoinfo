package videoinfo

import (
	"encoding/xml"
	"strconv"

	"gitlab.com/bazzz/dates"
)


// Film represents a film.
type Film struct {
	XmlName    xml.Name   `xml:"movie"`
	Title      string     `xml:"title"`
	Premiered  dates.Date `xml:"premiered"`
	Overview   string     `xml:"plot"`
	Genres     []string   `xml:"genre"`
	Rating     float64    `xml:"-"`
	Popularity float64    `xml:"-"`
	Revenue    int        `xml:"-"`
	Runtime    int        `xml:"-"`
	Votes      int        `xml:"-"`
	Language   string     `xml:"-"`
	Poster     string     `xml:"-"`
	Background string     `xml:"-"`
	Budget     int        `xml:"-"`
	TMDB       int        `xml:"-"`
}

// NFO returns this film's nfo file xml represention.
func (f Film) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(f, "", "\t")
	result := []byte(xml.Header)
	result = append(result, data...)
	return result, err
}

// Filename returns a string suitable to be used as a filename for this film '<title> (year)'.
func (f Film) Filename() string {
	name := f.Title
	if f.Premiered.Year() > 0 {
		name += " (" + strconv.Itoa(f.Premiered.Year()) + ")"
	}
	return stringToFilename(name)
}

func (f Film) TitleAndYear() string {
	result := f.Title
	if f.Premiered.Year() > 0 {
		result += " " + strconv.Itoa(f.Premiered.Year())
	}
	return result
}
